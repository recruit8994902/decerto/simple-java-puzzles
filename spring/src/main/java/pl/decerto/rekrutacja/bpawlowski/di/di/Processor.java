package pl.decerto.rekrutacja.bpawlowski.di.di;

public interface Processor {
    String getName();
}
