package pl.decerto.rekrutacja.bpawlowski.di.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class Bean2Configuration {

    @Bean
    ProcessorA processorA1() {
        return new ProcessorA("A1");
    }

    @Bean
    Processor processorA2() {
        return new ProcessorA("A2");
    }

    @Bean
    ProcessingService processingService(ProcessorA processor) {
        return new ProcessingService(processor);
    }
}
