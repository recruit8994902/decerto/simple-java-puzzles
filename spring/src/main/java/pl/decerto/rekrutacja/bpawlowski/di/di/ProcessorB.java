package pl.decerto.rekrutacja.bpawlowski.di.di;

import org.springframework.stereotype.Component;

public class ProcessorB implements Processor {
    private final String name;

    public ProcessorB(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
