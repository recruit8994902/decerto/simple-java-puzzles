package pl.decerto.rekrutacja.bpawlowski.concurrency;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class BeanConfig {

    @Bean
    @Primary
    Processor asyncProcessor() {
        return new AsyncProcessor();
    }

    @Bean
    Processor syncProcessor() {
        return new SyncProcessor();
    }
}
