package pl.decerto.rekrutacja.bpawlowski.di.di;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Bean1Configuration {

    @Bean
    Processor processorA() {
        return new ProcessorA("A");
    }

    @Bean
    Processor processorB() {
        return new ProcessorB("B");
    }

    @Bean
    ProcessingService processingService(Processor processor) {
        return new ProcessingService(processor);
    }
}
