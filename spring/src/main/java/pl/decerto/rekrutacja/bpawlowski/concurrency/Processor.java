package pl.decerto.rekrutacja.bpawlowski.concurrency;

public interface Processor {
    void process();
}
