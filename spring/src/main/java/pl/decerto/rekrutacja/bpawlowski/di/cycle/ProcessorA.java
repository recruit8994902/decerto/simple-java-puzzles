package pl.decerto.rekrutacja.bpawlowski.di.cycle;

import org.springframework.stereotype.Component;

@Component
public class ProcessorA {
    private ProcessorB processorB;

    public ProcessorA(ProcessorB processorB) {
        this.processorB = processorB;
    }
}
