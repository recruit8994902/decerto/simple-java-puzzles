package pl.decerto.rekrutacja.bpawlowski.di.cycle;

import org.springframework.stereotype.Component;

@Component
public class ProcessorB {
    private ProcessorA processorA;

    public ProcessorB(ProcessorA processorA) {
        this.processorA = processorA;
    }
}
