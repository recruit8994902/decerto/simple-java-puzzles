package pl.decerto.rekrutacja.bpawlowski.concurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringServiceStarter {
    public static void main(String[] args) {
        final var ctx = new SpringApplication(SpringServiceStarter.class).run(args);

        final var processor = ctx.getBean(Processor.class);

        processor.process();
        ctx.close();
    }
}