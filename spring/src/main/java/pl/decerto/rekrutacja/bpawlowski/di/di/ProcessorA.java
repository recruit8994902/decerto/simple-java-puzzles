package pl.decerto.rekrutacja.bpawlowski.di.di;

public class ProcessorA implements Processor {
    private final String name;

    public ProcessorA(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
