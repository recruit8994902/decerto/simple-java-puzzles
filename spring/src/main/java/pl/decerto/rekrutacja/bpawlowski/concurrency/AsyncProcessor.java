package pl.decerto.rekrutacja.bpawlowski.concurrency;

import org.springframework.scheduling.annotation.Async;

public class AsyncProcessor implements Processor {

    @Async
    public void process() {
        System.out.println("I'm processing asynchronously!");
    }
}
