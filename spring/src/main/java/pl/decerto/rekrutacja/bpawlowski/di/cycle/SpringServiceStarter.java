package pl.decerto.rekrutacja.bpawlowski.di.cycle;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringServiceStarter {
    public static void main(String[] args) {
        new SpringApplication(SpringServiceStarter.class).run(args);
    }
}