package pl.decerto.rekrutacja.bpawlowski.di.di;

public class ProcessingService {
    private final Processor processor;

    public ProcessingService(Processor processor) {
        this.processor = processor;
    }
}
