package pl.decerto.rekrutacja.bpawlowski.concurrency;

public class SyncProcessor implements Processor {
    @Override
    public void process() {
        System.out.println("I'm processing synchronously");
    }
}
