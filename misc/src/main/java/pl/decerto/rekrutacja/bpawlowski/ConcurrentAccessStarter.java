package pl.decerto.rekrutacja.bpawlowski;

import static pl.decerto.rekrutacja.bpawlowski.ConcurrencyUtils.sleep;

class Storage {
    int value = 0;
}

class StorageModifier implements Runnable {
    static int counter = 0;
    private int id;
    private Storage storage;
    private long delay;

    public StorageModifier(Storage storage, long delay) {
        id = counter++;
        this.storage = storage;
        this.delay = delay;
    }

    @Override
    public void run() {
        while (true) {
            storage.value++;
            System.out.println("[%s] Modifying storage value: '%s'".formatted(id, storage.value));
            sleep(delay);
        }
    }
}

public class ConcurrentAccessStarter {
    public static void main(String[] args) {
        final var delay = 1000;
        final var storage = new Storage();

        new Thread(new StorageModifier(storage, delay)).start();
        new Thread(new StorageModifier(storage, delay)).start();
    }
}
