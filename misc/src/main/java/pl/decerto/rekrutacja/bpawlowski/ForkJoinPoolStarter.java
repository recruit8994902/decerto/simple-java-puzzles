package pl.decerto.rekrutacja.bpawlowski;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.ForkJoinWorkerThread;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ForkJoinPoolStarter {
    private static final AtomicInteger index = new AtomicInteger(0);
    private static final ForkJoinPool.ForkJoinWorkerThreadFactory FJP_WORKER_THREAD_FACTORY = pool -> {
        final ForkJoinWorkerThread worker = ForkJoinPool.defaultForkJoinWorkerThreadFactory.newThread(pool);
        worker.setName("my-task-pool-" + worker.getPoolIndex() + "-" + index.getAndIncrement());
        return worker;
    };

    public static void main(String[] args) throws Exception {
        final var tasksCount = 1000;
        final var start = System.currentTimeMillis();
        System.out.println();
        final var workers = 100;
        final var fjp = new ForkJoinPool(workers, FJP_WORKER_THREAD_FACTORY, null, false);

        final var i = new AtomicInteger(0);
        final var counter = new AtomicInteger(0);
        final Runnable task = () -> {
            final var ifjp = fjp;
            final var id = i.getAndIncrement();
            sleep(1000);
            final var x = ifjp.toString();
            System.out.println("Finished: %s by: %s".formatted(id, Thread.currentThread().getName()));
            counter.incrementAndGet();
        };

        ForkJoinTask submittedTask = null;

        IntStream.of(new int[tasksCount])
                .parallel()
                .forEach(e -> fjp.submit(task));

        fjp.shutdown();
        fjp.awaitTermination(20, TimeUnit.DAYS);
        final var end = System.currentTimeMillis();

        System.out.println("elapsed time: %s, finished tasks: %s".formatted(end - start, counter.get()));

        index.set(0);

    }

    private static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
