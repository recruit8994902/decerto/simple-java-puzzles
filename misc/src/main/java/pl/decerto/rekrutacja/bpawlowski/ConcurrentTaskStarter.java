package pl.decerto.rekrutacja.bpawlowski;

import static pl.decerto.rekrutacja.bpawlowski.ConcurrencyUtils.sleep;

public class ConcurrentTaskStarter {
    public static void main(String[] args) {
        final Runnable runnable1 = () -> {
            while(true) {
                System.out.println("Serdecznie, jestem runnable1 8)");
                sleep(2000);
            }
        };
        final Runnable runnable2 = () -> {
            System.out.println("Serdecznie, jestem runnable2 8)");
            sleep(100);
        };

        new Thread(runnable1).run();
        new Thread(runnable2).run();
    }
}
