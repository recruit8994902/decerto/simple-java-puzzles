package pl.decerto.rekrutacja.bpawlowski;

/**
 * Modify this program so its capable of writing previously read file contents to standard output so
 * "special" latin(polish) characters are displayed properly
 */
public class TextFileStarter {
    public static void main(String[] args) {
        final var file = ClasspathUtils.load("pastaw.txt");

        System.out.println(file);
    }
}