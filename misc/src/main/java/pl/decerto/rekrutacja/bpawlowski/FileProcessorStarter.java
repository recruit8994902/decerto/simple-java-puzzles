package pl.decerto.rekrutacja.bpawlowski;

import java.io.FileInputStream;

/**
 * Modify this program (if needed) so it's capable of writing contents
 * previously read from given FileInputStream to standard output
 */
public class FileProcessorStarter {
    public static void main(String[] args) throws Exception {
        final var fileLocation = args[0];

        final var stream = new FileInputStream(fileLocation);
        final var file = new MyFile(stream.readAllBytes());

        System.out.println(new String(file.data));
    }
}

class MyFile {
    byte[] data;

    public MyFile(byte[] data) {
        this.data = data;
    }
}
