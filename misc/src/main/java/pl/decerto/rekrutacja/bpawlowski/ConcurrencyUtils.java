package pl.decerto.rekrutacja.bpawlowski;

public class ConcurrencyUtils {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            System.out.println("Interrupted");
        }
    }
}
