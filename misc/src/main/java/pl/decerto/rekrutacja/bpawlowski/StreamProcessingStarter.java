package pl.decerto.rekrutacja.bpawlowski;

import java.util.List;
import java.util.function.Function;

import static pl.decerto.rekrutacja.bpawlowski.ConcurrencyUtils.sleep;

public class StreamProcessingStarter {

    public static void main(String[] args) {
        final Function<String, String> processor = (x) -> {
            sleep(1000);
            return x + " ";
        };

        final var start = System.currentTimeMillis();
        List.of("This", "is", "simple", "processing", "example")
                .stream()
                .map(processor)
                .forEach(System.out::print);

        final var stop = System.currentTimeMillis();
        final var diff = stop - start;

        System.out.println("\nProcessing time: " + diff);
    }


}