package pl.decerto.rekrutacja.bpawlowski;

import java.nio.charset.StandardCharsets;

import static java.nio.charset.StandardCharsets.UTF_8;

final class Immutable {
    byte[] data;

    public Immutable(byte[] data) {
        this.data = data;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}

public class ImmutabilityStarter {

    public static void main(String[] args) {
        final var data = "string consists of bytes".getBytes(UTF_8);
        final var immutable1 = new Immutable(data);


        System.out.println(new String(immutable1.data));
    }
}
