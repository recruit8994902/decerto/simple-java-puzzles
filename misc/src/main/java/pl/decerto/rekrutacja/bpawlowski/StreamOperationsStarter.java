package pl.decerto.rekrutacja.bpawlowski;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;

import static pl.decerto.rekrutacja.bpawlowski.ConcurrencyUtils.sleep;

public class StreamOperationsStarter {

    public static void main(String[] args) {
        Stream.of("This", "is", "simple", "processing", "example")
                .map(e -> "x " + e + " x")
                .peek(e -> System.out.println(e));

    }
}