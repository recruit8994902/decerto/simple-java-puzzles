package pl.decerto.rekrutacja.bpawlowski;


import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import static java.util.Optional.ofNullable;

public class ClasspathUtils {

    public static InputStream streamResource(String resource) {
        return ClasspathUtils.class.getClassLoader()
                .getResourceAsStream(resource);
    }

    public static byte[] loadBytes(String resource) {
        return ofNullable(streamResource(resource))
                .map(ClasspathUtils::readAllBytes)
                .orElseThrow(() -> new RuntimeException("Resource was null '%s'".formatted(resource)));
    }

    private static byte[] readAllBytes(InputStream inputStream) {
        try {
            return inputStream.readAllBytes();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String load(String resource) {
        return load(resource, Charset.forName("windows-1250"));
    }

    public static String load(String resource, Charset charset) {
        return ofNullable(streamResource(resource))
                .map(e -> MyIOUtils.toString(e, charset))
                .orElseThrow(() -> new RuntimeException("Resource was null '%s'".formatted(resource)));
    }
}
