package pl.decerto.rekrutacja.bpawlowski;

import org.apache.commons.io.IOUtils;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class MyIOUtils {

    public static String toString(InputStream inputStream, Charset charset) {
        try {
            return IOUtils.toString(inputStream, charset);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String toString(InputStream inputStream) {
        return toString(inputStream, StandardCharsets.UTF_8);
    }
}
