package pl.decerto.rekrutacja.bpawlowski;

import static pl.decerto.rekrutacja.bpawlowski.ClasspathUtils.loadBytes;

public class ClassLoadingStarter {
    public static void main(String[] args) throws Exception {
        final var compiledClass = loadBytes("CompilableClass.class");

        var cl1 = new InMemoryClassLoader(ClassLoadingStarter.class.getClassLoader());
        Class<CompilableClass> cl1LoadedClass = cl1.load("pl.decerto.rekrutacja.bpawlowski.CompilableClass", compiledClass);
        var cl2 = new InMemoryClassLoader(ClassLoadingStarter.class.getClassLoader());
        Class<CompilableClass> cl2LoadedClass = cl2.load("pl.decerto.rekrutacja.bpawlowski.CompilableClass", compiledClass);

        var cl1LoadedClassInstance = cl1LoadedClass.getConstructors()[0].newInstance();
        var cl2LoadedClassInstance = cl2LoadedClass.getConstructors()[0].newInstance();


        var cl2LoadedClassInstanceAfterCast = cl1LoadedClass.cast(cl2LoadedClassInstance);

        CompilableClass compilableClass = (CompilableClass) cl1LoadedClassInstance;
        compilableClass.hello();
    }
}























//        cl1LoadedClassInstance = cl2LoadedClassInstance;
//        cl2LoadedClassInstance = tmp;


//        System.out.println("cl1LoadedClass: '%s'".formatted(cl1LoadedClass.getName()));
//        System.out.println("cl2LoadedClass: '%s'".formatted(cl2LoadedClass.getName()));
//        System.out.println("cl1LoadedClass eq cl2LoadedClass: '%s'".formatted(cl1LoadedClass.equals(cl2LoadedClass)));