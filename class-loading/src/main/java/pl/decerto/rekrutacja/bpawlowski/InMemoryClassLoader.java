package pl.decerto.rekrutacja.bpawlowski;

import java.net.URL;
import java.net.URLClassLoader;

public class InMemoryClassLoader extends URLClassLoader {

    public InMemoryClassLoader(ClassLoader parent) {
        super(new URL[0], parent);
    }

    public InMemoryClassLoader() {
        super(new URL[0]);
    }

    protected <T> Class<T> load(String name, byte[] data) {
        return (Class<T>) defineClass(name, data, 0, data.length);
    }
}