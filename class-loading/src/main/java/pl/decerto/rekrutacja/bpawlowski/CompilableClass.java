package pl.decerto.rekrutacja.bpawlowski;

import java.io.Serializable;

public class CompilableClass implements Serializable {
    public void hello() {
        System.out.println("Hello!");
    }
}
